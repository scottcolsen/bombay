$(document).ready(function() {
    refreshApi();
});

function refreshApi() {
    $('#specials').load("http://bombay-cms.heroku.com/api/specials.html");
    $('#shows').load("http://bombay-cms.heroku.com/api/shows.html");
    $('#local').load("http://bombay-cms.heroku.com/api/local.html");
    $('#featured').load("http://bombay-cms.heroku.com/api/featured.html");
    $("#genres_list").load("http://bombay-cms.heroku.com/api/genres.html", function() {
        $("#genres_list").listview('refresh');
    });
}

function getBands(genre_id) {
    $('#bands_by_genre').load("http://bombay-cms.heroku.com/api/bands.html?genre_id=" + genre_id);
}
function getBand(band_id) {
    $('#band').load("http://bombay-cms.heroku.com/api/band.html?band_id=" + band_id);
}
function getEvent(event_id) {
    $('#event').load("http://bombay-cms.heroku.com/api/event.html?event_id=" + event_id);
}